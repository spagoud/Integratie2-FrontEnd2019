import { Component, OnInit } from '@angular/core';
import {GamelobbyService} from '../../services/gamelobby/gamelobby.service';
import {GameLobby} from '../../models/GameLobby';
import {Router} from '@angular/router';

@Component({
  selector: 'app-serverlobby',
  templateUrl: './serverlobby.component.html',
  styleUrls: ['./serverlobby.component.css']
})
export class ServerlobbyComponent implements OnInit {

  lobbies: GameLobby[];

  constructor(private gameLobbyService: GamelobbyService,
              private router: Router) { }

  ngOnInit() {
    this.getLobbies();
  }

  getLobbies(): void {
    this.gameLobbyService.getAllLobbies()
      .subscribe(lobbies => this.lobbies = lobbies);
  }

  createLobby() {
    this.router.navigate(['game', 'new']);
  }
}
