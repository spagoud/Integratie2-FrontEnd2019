import {Component, Input, OnInit} from '@angular/core';
import {GameLobby} from '../../../models/GameLobby';
import {LoginService} from '../../../services/login/login.service';
import {GamelobbyService} from '../../../services/gamelobby/gamelobby.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})
export class LobbyComponent implements OnInit {

  @Input() lobby: GameLobby;

  constructor(private lobbyService: GamelobbyService, private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  joinLobby() {
    this.lobbyService.joinLobby(this.lobby.id, this.loginService.user.email).subscribe();
    this.router.navigate(['lobby', this.lobby.id]);
  }

  spectateLobby() {
    
  }
}
