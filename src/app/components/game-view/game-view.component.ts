import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UnitySenderService} from '../../services/unity-sender/unity-sender.service';
import {GamelogicService} from '../../services/gamelogic/gamelogic.service';
import {UnityRecieverService} from '../../services/unity-reciever/unity-reciever.service';
import {LoginService} from '../../services/login/login.service';
import {environment} from '../../../environments/environment';

const SockJs = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'app-game-view',
  templateUrl: './game-view.component.html',
  styleUrls: ['./game-view.component.css']
})
export class GameViewComponent implements OnInit {

  socket;
  stompClient;
  private serverUrl = environment.socketURL;
  // private serverUrl = 'http://localhost:8082/ws';

  constructor(private activatedRoute: ActivatedRoute,
              private unitySender: UnitySenderService,
              private unityReceiver: UnityRecieverService,
              private gameLogic: GamelogicService) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const lobbyId = parseInt(params.get('id'), 10);
      this.unityReceiver.gameStartRequested.subscribe(start => {
        if (start) {
          this.socket = new SockJs(this.serverUrl);
          this.stompClient = Stomp.over(this.socket);
          this.stompClient.connect({}, (frame) => {
            this.stompClient.subscribe('/lobby/chat/' + lobbyId, (message) => {
              console.log(message);
              this.unitySender.SendChatMessage(message.body);
            });
            this.stompClient.subscribe('/lobby/' + lobbyId + '/game', (message) => {
              console.log(message);
              this.unitySender.setUser();
              this.unitySender.setGame(message.body);
            });
            this.stompClient.subscribe('/lobby/' + lobbyId + '/event', (message) => {
              console.log(message);
              this.unitySender.SendEvent(message.body);
            });
            this.stompClient.subscribe('/lobby/' + lobbyId + '/gameend', (message) => {
              console.log(message);
              this.unitySender.SendEnd(message.body);
            });
            this.gameLogic.ready(lobbyId).subscribe();
          });
        }
      });

    });
  }
}
