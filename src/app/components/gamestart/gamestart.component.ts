import {Component, Input, OnInit} from '@angular/core';
import {GamestartService} from '../../services/gamestart/gamestart.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GameLobby} from '../../models/GameLobby';
import {Router} from '@angular/router';
import {GamelogicService} from '../../services/gamelogic/gamelogic.service';
import {LoginService} from '../../services/login/login.service';
import {ActionService} from '../../services/action/action.service';

@Component({
  selector: 'app-gamestart',
  templateUrl: './gamestart.component.html',
  styleUrls: ['./gamestart.component.css']
})
export class GamestartComponent implements OnInit {


  @Input()
  model: FormGroup;
  @Input() lobby: GameLobby;

  constructor(private gamestartservice: GamestartService, private gamelogicservice: GamelogicService, private formBuiler: FormBuilder,
              private router: Router, private actionservice: ActionService) {
  }

  ngOnInit() {
    this.model = this.formBuiler.group({
      'lobbyName': ['', [Validators.required]],
      'inviteOnly': ['', ]
    });
  }

  lobbyStart() {
    const gameLobby = new GameLobby();
    gameLobby.name = this.model.value.lobbyName;
    gameLobby.inviteOnly = this.model.value.inviteOnly;
    this.gamestartservice.startLobby(gameLobby).subscribe((lobby) => {
      this.router.navigate(['lobby/', lobby.id]);
    });
  }
}
