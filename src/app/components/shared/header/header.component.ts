import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../services/login/login.service';
import {AuthService} from '../../../services/auth/auth.service';
import {User} from '../../../models/User';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLoggedin = false;
  loggedInUser: User | null;

  constructor(private loginService: LoginService,
              private authService: AuthService,
              private router: Router) {
    this.router.events.subscribe(() => this.getLoggedInUser());
    this.getLoggedInUser();
  }

  ngOnInit() {

  }

  private getLoggedInUser() {
    this.isLoggedin = this.authService.isAuthenticated();
    this.loggedInUser = this.loginService.user;
  }
}
