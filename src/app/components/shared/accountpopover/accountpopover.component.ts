import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../services/login/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-accountpopover',
  templateUrl: './accountpopover.component.html',
  styleUrls: ['./accountpopover.component.css']
})
export class AccountpopoverComponent implements OnInit {

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
  }

  logout() {
    this.loginService.logout().subscribe(() => {
      this.router.navigate(['/home']);
      
    });
  }
}
