import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/User';
import {CustomValidators} from '../../models/CustomValidators';
import {RegisterService} from '../../services/register/register.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private registerService: RegisterService,
              private router: Router) {
  }

  ngOnInit() {
    this.model = this.formBuilder.group({
      'firstname': ['', [Validators.required]],
      'lastname': ['', [Validators.required]],
      'email': ['', [Validators.required, Validators.email]],
      'password': ['', [Validators.required]],
      'passwordConfirm': ['', [Validators.required]],
      'TFA': [false, []],
      'tec': [false, [Validators.required]]
    }, {validators: [CustomValidators.identical(['password', 'passwordConfirm'])]});
  }

  registerUser() {
    if (this.model.valid) {
      const values = this.model.value;
      const user = new User();
      user.firstName = values.firstname;
      user.lastName = values.lastname;
      user.email = values.email;
      user.password = values.password;
      user.matchingPassword = values.passwordConfirm;
      user.isUsing2FA = values.TFA;
      this.registerService.register(user).subscribe((response: Response) => {
        this.router.navigate(['registered']);
      }, (error) => {
        console.log(error);
      });
    }
  }
}
