import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {RegisterService} from '../../../services/register/register.service';

@Component({
  selector: 'app-confirm-register',
  templateUrl: './confirm-register.component.html',
  styleUrls: ['./confirm-register.component.css']
})
export class ConfirmRegisterComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private registerService: RegisterService) {
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe((params: ParamMap) => {
      if (params.has('token')) {
        this.registerService.confirm(params.get('token')).subscribe(() => {
          this.router.navigate(['/login']);
        });
      }
    });
  }

}
