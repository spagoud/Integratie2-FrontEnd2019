import {Component, OnInit} from '@angular/core';
import {GamelobbyService} from '../../services/gamelobby/gamelobby.service';
import {ActivatedRoute} from '@angular/router';
import {UnityRecieverService} from '../../services/unity-reciever/unity-reciever.service';
import {UnitySenderService} from '../../services/unity-sender/unity-sender.service';

@Component({
  selector: 'app-replay-game-view',
  templateUrl: './replay-game-view.component.html',
  styleUrls: ['./replay-game-view.component.css']
})
export class ReplayGameViewComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
              private gameLobbyService: GamelobbyService,
              private unityReceiver: UnityRecieverService,
              private unitySender: UnitySenderService) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const lobbyId = parseInt(params.get('id'), 10);
      this.unityReceiver.gameStartRequested.subscribe(start => {
        if (start) {
          this.gameLobbyService.getReplay(lobbyId).subscribe((data: any) => {
            this.unitySender.setGame(data.initialGameDataJson);
            data.actionMessages.forEach(message => {
              this.unitySender.SendEvent(JSON.stringify(message));
            });
          });
        }
      });
    });
  }

}
