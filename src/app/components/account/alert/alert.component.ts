import {Component, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';
import {LoginService} from '../../../services/login/login.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  emailaddress: string;
  @Output() eventEmitter = new EventEmitter();

  constructor(private loginService: LoginService, private host: ElementRef<HTMLElement>) { }

  ngOnInit() {
    this.emailaddress = this.loginService.user.email;
    this.container.style.animation = 'snackbarIn 0.3s';
  }

  get container(): HTMLElement {
    return this.host.nativeElement.querySelector('.alert') as HTMLElement;
  }

  close() {
    this.container.style.animation = 'snackbarOut 0.3s';
    this.eventEmitter.emit(false);
  }
}
