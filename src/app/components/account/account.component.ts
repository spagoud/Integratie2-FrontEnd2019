import {Component, OnInit} from '@angular/core';
import {UserprofileService} from '../../services/userprofile/userprofile.service';
import {UserService} from '../../services/user/user.service';
import {LoginService} from '../../services/login/login.service';
import {User} from '../../models/User';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  userProfile: User[];
  showAlert = false;
  disabledForm = true;
  editbtntext = 'edit';
  userEmail: string;
  username: string;

  constructor(private userProfileService: UserprofileService, private userService: UserService, private loginService: LoginService) {
  }

  ngOnInit() {
    this.getUserProfile();
  }

  getUserProfile() {
    this.userProfileService.getUserprofile().subscribe(userprofile => this.userProfile = userprofile);
    this.userEmail = this.loginService.user.email;
    this.username = this.loginService.user.username;
  }

  resetPassword() {
    this.userService.resetPassword().subscribe();
    this.showAlert = true;
  }

  closeAlert(event) {
    this.showAlert = event;
  }

  toggleEditBtn() {
    if (this.editbtntext === 'edit') {
      this.editbtntext = 'save';
      this.disabledForm = false;
    } else {
      this.editbtntext = 'edit';
      this.disabledForm = true;
    }
  }
}
