import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserprofileService} from '../../services/userprofile/userprofile.service';
import {Router} from '@angular/router';
import {LoginService} from '../../services/login/login.service';
import {User} from '../../models/User';

@Component({
  selector: 'app-new-profile',
  templateUrl: './new-profile.component.html',
  styleUrls: ['./new-profile.component.css']
})
export class NewProfileComponent implements OnInit {

  model: FormGroup;

  constructor(private formbuilder: FormBuilder,
              private userprofileService: UserprofileService,
              private router: Router,
              private loginService: LoginService) { }

  ngOnInit() {
    this.model = this.formbuilder.group({
      'username': ['', [Validators.required]],
      'profilepicture': ['']
    });
  }

  submitProfile() {
    if (this.model.valid) {
      const values = this.model.value;
      const userprofile = new User();
      userprofile.username = values.username;
      userprofile.profilePicture = values.profilepicture;
      this.userprofileService.createUserProfile(userprofile).subscribe((response: Response) => {
        this.loginService.user = userprofile;
        this.router.navigate(['']);
      }, (error) => {
        console.log(error);
      });
    }
  }
}
