import {Component, OnInit, OnDestroy} from '@angular/core';
import {GamelobbyService} from '../../services/gamelobby/gamelobby.service';
import {GameLobby} from '../../models/GameLobby';
import {ActivatedRoute, Router} from '@angular/router';
import {GamelogicService} from '../../services/gamelogic/gamelogic.service';
import {JoinLobby} from '../../models/JoinLobby';
import {LoginService} from '../../services/login/login.service';
import {ActionService} from '../../services/action/action.service';
import {environment} from '../../../environments/environment';
import {ChatMessage, ChatMessageType} from '../../models/messages/ChatMessage';

const SockJs = require('sockjs-client');
const Stomp = require('stompjs');

@Component({
  selector: 'app-detailed-lobby',
  templateUrl: './detailed-lobby.component.html',
  styleUrls: ['./detailed-lobby.component.css']
})
export class DetailedLobbyComponent implements OnInit, OnDestroy {

  lobby: GameLobby;
  regdarDisplayed = false;
  liddaDisplayed = false;
  mialeeDisplayed = false;
  jozanDisplayed = false;
  socket;
  stompClient;
  listMessages: ChatMessage[] = [];
  message = new ChatMessage();
  gameState: string;

  private serverUrl = environment.socketURL;


  constructor(private gameLobbyService: GamelobbyService,
              private route: ActivatedRoute,
              private router: Router,
              private gameLogicService: GamelogicService,
              private loginService: LoginService,
              private actionService: ActionService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      if (paramMap.has('lobbyId')) {
        this.gameLobbyService.getLobby(paramMap.get('lobbyId')).subscribe((gameLobby: GameLobby) => {
          this.lobby = gameLobby;
          this.gameState = this.lobby.gameState;
          this.actionService.setLobbyId(this.lobby.id);
          // Sets which heroes are not yet chosen
          this.setHeroBooleans();
          this.socket = new SockJs(this.serverUrl);
          this.stompClient = Stomp.over(this.socket);
          this.stompClient.connect({}, (frame) => {
            this.stompClient.subscribe('/lobby/' + this.lobby.id + '/gamestart', (message) => {
              console.log(message);
              this.router.navigate(['game/' + this.lobby.id + '/play']);
            });
            this.stompClient.subscribe('/lobby/' + this.lobby.id + '/lobbyUpdate', (message) => {
              console.log(message);
              this.lobby = JSON.parse(message.body);
              this.setHeroBooleans();
            });
            this.stompClient.subscribe('/lobby/chat/' + this.lobby.id, (message) => {
              console.log(message);
              this.listMessages.push(JSON.parse(message.body));
            });
          });
        });
      }
    });
  }

  ngOnDestroy() {
    this.gameLobbyService.removeViewer(this.lobby.id, this.loginService.user.email);
  }

  startGame() {
    this.gameLogicService.start(+this.lobby.id).subscribe(); // +this.lobby.id casts string to number
  }

  removeLobby() {
    this.gameLobbyService.remove(this.lobby.id).subscribe(() => {
      this.router.navigate(['gameRooms']);
    });
  }

  setHeroBooleans() {
    for (const lobbyHero of this.lobby.heroes) {
      switch (lobbyHero.heroName) {
        case 'Regdar':
        case 'RegdarAI':
          this.regdarDisplayed = true;
          break;
        case 'Lidda':
        case 'LiddaAI':
          this.liddaDisplayed = true;
          break;
        case 'Mialee':
        case 'MialeeAI':
          this.mialeeDisplayed = true;
          break;
        case 'Jozan':
        case 'JozanAI':
          this.jozanDisplayed = true;
          break;
        default:
          console.log('heroName heeft geen overeenkomsten ... (dit is waarschijnlijk een fout)');
      }
    }
  }

  chooseHero(heroName: string) {
    const joinLobby = new JoinLobby();
    joinLobby.heroName = heroName;
    joinLobby.email = this.loginService.user.email;
    this.gameLobbyService.addPlayer(this.lobby.id, joinLobby).subscribe();
  }

  onEnter() {
    this.message.sender = this.loginService.user.username;
    this.message.chatMessageType = ChatMessageType.CHAT;
    this.actionService.SendChatMessage(JSON.stringify(this.message)).subscribe(() => this.message.content = '');
  }
}
