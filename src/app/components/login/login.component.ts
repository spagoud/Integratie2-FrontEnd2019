import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../services/login/login.service';
import {User} from '../../models/User';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: FormGroup;
  errorMessage: string;
  private code: string;

  constructor(private formbuilder: FormBuilder,
              private loginService: LoginService,
              private router: Router) {

  }

  ngOnInit() {
    // Username = email, Password = password, Code = 2FA (can be empty)
    this.model = this.formbuilder.group({
      'email': ['', [Validators.required, Validators.email]],
      'password': ['', [Validators.required]]
    });
  }

  submitLogin() {
    // Call the login service that will do the http post call to /login
    const user = new User();
    user.email = this.model.value.email;
    user.password = this.model.value.password;
    this.loginService.login(user);

  }

  loginWithFacebook() {
    this.loginService.loginWithFacebook().subscribe((url) => {
       window.location.href = url;
    });
  }
}
