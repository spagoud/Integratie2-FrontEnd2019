import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginService} from '../../../services/login/login.service';

@Component({
  selector: 'app-facebook-login',
  templateUrl: './facebook-login.component.html',
  styleUrls: ['./facebook-login.component.css']
})
export class FacebookLoginComponent implements OnInit {

  private code: string;

  constructor(private activatedRoute: ActivatedRoute,
              private loginService: LoginService) { }

  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe(paramMap => {
      paramMap.has('code');
      {
        this.code = paramMap.get('code');
        this.loginService.authenticateWithFacebook(this.code);
      }
    });
  }

}
