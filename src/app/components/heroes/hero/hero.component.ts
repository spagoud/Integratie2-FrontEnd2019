import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Hero} from '../../../models/Hero';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {GamestartService} from '../../../services/gamestart/gamestart.service';

@Component({
  selector: 'app-hero',
  animations: [
    trigger('flipArrow', [
      state('down', style({
        transform: 'rotate(360deg)'
      })),
      state('up', style({
        transform: 'rotate(180deg)'
      })),
      transition('down <=> up', [
        animate('200ms cubic-bezier(0.35, 0, 0.25, 1)')
      ])
    ]),
    trigger('fade', [
      state('in', style({
        opacity: 1
      })),
      state('out', style({
        opacity: 0
      })),
      transition('out <=> in', [
        animate('200ms cubic-bezier(0.35, 0, 0.25, 1)')
      ])
    ])
  ],
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {

  @Input() hero: Hero;
  @Input() selectedHero: string;
  @Output() selectedHeroEmitter: EventEmitter<any> = new EventEmitter();
  showEquipment = false;
  showWeapon = false;
  showArtifact = false;
  showExtra = false;
  showDescription = false;

  constructor(private _gamestartservice: GamestartService) {
  }

  ngOnInit() {
  }

  toggleWeapon() {
    this.showWeapon = !this.showWeapon;
    this.showArtifact = false;
    this.showExtra = false;
  }

  toggleArtifact() {
    this.showArtifact = !this.showArtifact;
    this.showWeapon = false;
    this.showExtra = false;
  }

  toggleExtra() {
    this.showExtra = !this.showExtra;
    this.showArtifact = false;
    this.showWeapon = false;
  }

  toggleDescription() {
    this.showDescription = !this.showDescription;
  }

  selectHero(heroName: string) {
    this._gamestartservice.setHero(heroName);
    this.selectedHeroEmitter.emit(heroName);
  }
}
