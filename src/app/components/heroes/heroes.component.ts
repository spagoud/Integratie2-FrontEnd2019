import {Component, ElementRef, HostBinding, HostListener, OnInit, ViewChild} from '@angular/core';
import {HeroService} from '../../services/hero/hero.service';
import {Hero} from '../../models/Hero';
import {animate, query, stagger, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-heroes',
  animations: [
    trigger('pageAnimation', [
      transition(':enter', [
        query('app-hero', [
          style({opacity: 0, transform: 'translateY(200px)'}),
          stagger(-50, [
            animate('500ms cubic-bezier(0.35, 0, 0.25, 1)', style({ opacity: 1, transform: 'none' }))
          ])
        ])
      ])
    ]),
  ],
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  @HostBinding('@pageAnimation')
  public animatePage = true;
  selectedHero: string;
  heroes: Hero[];

  constructor(private heroService: HeroService) {
  }

  ngOnInit() {
    this.getHeroes();
  }

  private getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }

  setSelectedHero(heroName: any) {
    this.selectedHero = heroName;
  }
}
