import { Component, OnInit } from '@angular/core';
import {StatisticService} from '../../services/statistic/statistic.service';
import {UserStatistics} from '../../models/UserStatistics';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {

  userStats: UserStatistics;
  timeRegdar: string;
  timeLidda: string;
  timeMialee: string;
  timeJozan: string;

  constructor(private statisticService: StatisticService) { }

  ngOnInit() {
    this.statisticService.getStatics().subscribe((resp: UserStatistics) => {
      this.userStats = resp;
      for (const characterStats of this.userStats.characterStatistics) {
        switch (characterStats.characterName.toUpperCase()) {
          case 'REGDAR':
            this.timeRegdar = this.transform(characterStats.timePlayed);
            break;
          case 'MIALEE':
            this.timeMialee = this.transform(characterStats.timePlayed);
            break;
          case 'JOZAN':
            this.timeJozan = this.transform(characterStats.timePlayed);
            break;
          case 'LIDDA':
            this.timeLidda = this.transform(characterStats.timePlayed);
            break;
        }
      }
    });
  }

  transform(value: number, args?: any): string {

    const hours: number = Math.floor(value / 60);
    const minutes: number = (value - hours * 60);

    if (hours < 10 && minutes < 10) {
      return '0' + hours + ' u 0' + (value - hours * 60) + ' min';
    }
    if (hours > 10 && minutes > 10) {
      return '0' + hours + ' u ' + (value - hours * 60) + ' min';
    }
    if (hours > 10 && minutes < 10) {
      return hours + ' u 0' + (value - hours * 60) + ' min';
    }
    if (minutes > 10) {
      return '0' + hours + ' u ' + (value - hours * 60) + ' min';
    }
  }
}
