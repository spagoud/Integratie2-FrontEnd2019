import {AfterViewInit, Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {UnitySenderService} from '../../services/unity-sender/unity-sender.service';

@Component({
  selector: 'app-unity-wrapper',
  templateUrl: './unity-wrapper.component.html',
  styleUrls: ['./unity-wrapper.component.css']
})
export class UnityWrapperComponent implements OnInit, AfterViewInit, OnDestroy{


  constructor(private unitySender: UnitySenderService) {
  }

  @HostListener('window:resize')
  resize() {
    const aspecRatio: number = 16 / 9;
    const canvas = <HTMLCanvasElement>document.getElementById('#canvas');
    const container = document.getElementById('gameContainer');
    if (canvas && container) {

      const maxWidth = window.innerWidth;
      const maxHeight = window.innerHeight;

      console.log('maxWidht', maxWidth);
      console.log('maxHeight', maxHeight);

      if ((maxWidth / maxHeight) > aspecRatio) {
        console.log('aspec on Height');
        canvas.width = maxHeight * aspecRatio;
        canvas.height = maxHeight;
      } else {
        console.log('aspec on Widht');
        canvas.width = maxWidth;
        canvas.height = maxWidth / aspecRatio;
      }

      /*canvas.style.width = '100%';
      canvas.height = canvas.width / this.aspecRatio;
      const tempWidth: number = canvas.clientWidth;
      const tempHeight: number = canvas.clientHeight;
      if ((tempHeight * this.aspecRatio) < tempWidth) {

        canvas.style.height = '100%';
        canvas.width = canvas.height * this.aspecRatio;

      }*/
    }
  }

  ngOnInit(): void {
    this.unitySender.startGame(this.resize);

  }

  ngAfterViewInit(): void {
    this.resize();
  }

  ngOnDestroy(): void {
    this.unitySender.stopGame();
  }


}
