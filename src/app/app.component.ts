import {Component} from '@angular/core';
import {UnityRecieverService} from './services/unity-reciever/unity-reciever.service';
import {UnitySenderService} from './services/unity-sender/unity-sender.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Integratie2-FrontEnd2019';

  constructor(private unityReciever: UnityRecieverService, private unitySender: UnitySenderService) {

  }
}
