import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';

import { AppRoutingModule } from './app-routing.module.tns';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UnityWrapperComponent } from './components/unity-wrapper/unity-wrapper.component';
import { HomeComponent } from './components/home/home.component';
import { RegisteredComponent } from './components/register/registered/registered.component';
import { ConfirmRegisterComponent } from './components/register/confirm-register/confirm-register.component';
import { HeroesComponent } from './components/heroes/heroes.component';
import { HeroComponent } from './components/heroes/hero/hero.component';
import { ServerlobbyComponent } from './components/serverlobby/serverlobby.component';
import { LobbyComponent } from './components/serverlobby/lobby/lobby.component';
import { AccountpopoverComponent } from './components/shared/accountpopover/accountpopover.component';
import { AccountComponent } from './components/account/account.component';
import { AlertComponent } from './components/account/alert/alert.component';
import { NewProfileComponent } from './components/new-profile/new-profile.component';
import { DetailedLobbyComponent } from './components/detailed-lobby/detailed-lobby.component';
import { FacebookLoginComponent } from './components/login/facebook-login/facebook-login.component';


// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    UnityWrapperComponent,
    HomeComponent,
    RegisteredComponent,
    ConfirmRegisterComponent,
    HeroesComponent,
    HeroComponent,
    ServerlobbyComponent,
    LobbyComponent,
    AccountpopoverComponent,
    AccountComponent,
    AlertComponent,
    NewProfileComponent,
    DetailedLobbyComponent,
    FacebookLoginComponent
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
