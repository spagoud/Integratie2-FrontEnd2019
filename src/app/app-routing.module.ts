import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {HomeComponent} from './components/home/home.component';
import {RegisteredComponent} from './components/register/registered/registered.component';
import {ConfirmRegisterComponent} from './components/register/confirm-register/confirm-register.component';
import {HeroesComponent} from './components/heroes/heroes.component';
import {GamestartComponent} from './components/gamestart/gamestart.component';
import {AuthGuardService} from './services/auth-guard/auth-guard.service';
import {GameViewComponent} from './components/game-view/game-view.component';
import {ServerlobbyComponent} from './components/serverlobby/serverlobby.component';
import {RulesComponent} from './components/rules/rules.component';
import {AccountComponent} from './components/account/account.component';
import {NewProfileComponent} from './components/new-profile/new-profile.component';
import {DetailedLobbyComponent} from './components/detailed-lobby/detailed-lobby.component';
import {FacebookLoginComponent} from './components/login/facebook-login/facebook-login.component';
import {ReplayGameViewComponent} from './components/replay-game-view/replay-game-view.component';
import {StatisticComponent} from './components/statistic/statistic.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {path: 'register', component: RegisterComponent},
  {path: 'registered', component: RegisteredComponent},
  {path: 'user/registrationConfirm', component: ConfirmRegisterComponent},
  {path: 'heroes', component: HeroesComponent, canActivate: [AuthGuardService]},
  {path: 'rules', component: RulesComponent},
  {
    path: 'game', children: [
      {path: 'new', component: GamestartComponent, canActivate: [AuthGuardService]},
      {
        path: ':id', children: [
          {path: 'replay', component: ReplayGameViewComponent, canActivate: [AuthGuardService]},
          {path: 'play', component: GameViewComponent, canActivate: [AuthGuardService]},
        ]
      }
    ]
  },
  {path: 'gameRooms', component: ServerlobbyComponent, canActivate: [AuthGuardService]},
  {path: 'home', redirectTo: ''},
  {path: 'account', component: AccountComponent, canActivate: [AuthGuardService]},
  {path: 'newProfile', component: NewProfileComponent, canActivate: [AuthGuardService]},
  {path: 'lobby/:lobbyId', component: DetailedLobbyComponent, canActivate: [AuthGuardService]},
  {path: 'facebook', component: FacebookLoginComponent},
  {path: 'stats', component: StatisticComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
