import {User} from './User';

export class LobbyHero {
  userProfile: User;
  heroName: string;
  heroImage: string;
  type: string;
}
