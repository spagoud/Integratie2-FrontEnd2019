export class CharacterStatistics {
  characterName: string;
  timePlayed: number;
  monstersKilled: number;
  deaths: number;
  score: number;
  trapsActivated: number;
  trapsDismantled: number;
  bossesKilled: number;
  damageDone: number;
  damageTaken: number;
}
