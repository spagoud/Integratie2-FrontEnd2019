import {Item} from './Item';
import {Tile} from './Tile';

export class Door {
  name: String;
  tile1: Tile;
  tile2: Tile;
  enabled: boolean;
  locked: boolean;
}
