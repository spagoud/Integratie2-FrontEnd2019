import {Item} from './Item';

export class Equipment {
  weapon: Item;
  artifact: Item;
  extra: Item;
}
