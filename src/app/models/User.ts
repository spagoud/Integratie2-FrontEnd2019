export class User {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  password: string;
  matchingPassword: string;
  isUsing2FA: boolean;
  profilePicture: any;
}
