import {CharacterStatistics} from './CharacterStatistics';

export class UserStatistics {
  score: number;
  fastestClearingTime: number;
  profileLevel: number;
  characterStatistics: CharacterStatistics[];
}
