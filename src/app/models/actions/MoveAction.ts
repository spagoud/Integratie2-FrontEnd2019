import {Tile} from '../Tile';

export class MoveAction {
  heroName: string;
  movementPath: Tile[];
}
