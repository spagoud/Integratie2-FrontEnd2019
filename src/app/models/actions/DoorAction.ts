import {Door} from '../Door';

export class DoorAction {
  heroName: string;
  door: Door;
}
