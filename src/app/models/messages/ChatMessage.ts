export enum ChatMessageType {
  INFO,
  CHAT,
  JOIN,
  LEAVE
}

export class ChatMessage {
  sender: string;
  chatMessageType: ChatMessageType;
  content: string;
}
