export class Item {
  name: string;
  description: string;
  img: string;
  level: number;
  startersEquipment: boolean;
  type: string;
}
