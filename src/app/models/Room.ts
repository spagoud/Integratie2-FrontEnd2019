import {Tile} from './Tile';

export class Room {
  id: number;
  name: string;
  tiles: Tile[];
  monsters: any[];
  traps: any[];
  chests: any[];
  doors: any[];
}
