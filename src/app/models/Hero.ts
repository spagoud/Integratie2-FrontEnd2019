import {Equipment} from './Equipment';

export class Hero {
  symbolImg: string;
  name: string;
  description: string;
  armor: number;
  move: number;
  maxHealthPoints: number;
  maxMagicPoints: number;
  type: string;
  inventorySpace: number;
  startEquipment: Equipment;
}
