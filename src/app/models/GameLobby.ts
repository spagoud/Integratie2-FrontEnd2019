import {LobbyHero} from './LobbyHero';

export class GameLobby {
  id: string;
  name: string;
  lobbyOwner: string;
  heroes: LobbyHero[];
  viewers: string[];
  heroName: string;
  inviteOnly: boolean;
  playerCount: number;
  gameState: string;
}
