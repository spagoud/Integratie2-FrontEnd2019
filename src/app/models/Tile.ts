export class Tile {
  id: number;
  x: number;
  y: number;
  tileType: TileType;
}

export enum TileType {
  TOP, LEFT, RIGHT, BOTTOM, EMPTY, PILLAR,
  TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, FLOOR
}
