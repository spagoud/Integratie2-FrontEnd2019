import {AbstractControl, ValidatorFn} from '@angular/forms';

export class CustomValidators {
  static identical(controlNames: string[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      if (controlNames.map((name: string) => {
        return control.get(name).value;
      }).every(arg => arg === control.get(controlNames[0]).value)) {
        return {NotEqual: true};
      }
      return null;
    };
  }
}
