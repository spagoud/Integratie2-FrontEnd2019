import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RegisteredComponent} from './components/register/registered/registered.component';
import {JwtInterceptor} from './services/interceptors/jwt-interceptor';
import {ConfirmRegisterComponent} from './components/register/confirm-register/confirm-register.component';
import {HeroesComponent} from './components/heroes/heroes.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {GamestartComponent} from './components/gamestart/gamestart.component';
import {HeroComponent} from './components/heroes/hero/hero.component';
import {JwtModule} from '@auth0/angular-jwt';

import { GameViewComponent } from './components/game-view/game-view.component';
import { ServerlobbyComponent } from './components/serverlobby/serverlobby.component';
import { LobbyComponent } from './components/serverlobby/lobby/lobby.component';
import {NgbModule, NgbPopoverModule} from '@ng-bootstrap/ng-bootstrap';
import { AccountpopoverComponent } from './components/shared/accountpopover/accountpopover.component';
import { RulesComponent } from './components/rules/rules.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UnityWrapperComponent } from './components/unity-wrapper/unity-wrapper.component';
import { HomeComponent } from './components/home/home.component';
import { AccountComponent } from './components/account/account.component';
import { AlertComponent } from './components/account/alert/alert.component';
import { NewProfileComponent } from './components/new-profile/new-profile.component';
import { DetailedLobbyComponent } from './components/detailed-lobby/detailed-lobby.component';
import {AuthGuardService} from './services/auth-guard/auth-guard.service';
import { FacebookLoginComponent } from './components/login/facebook-login/facebook-login.component';
import { StatisticComponent } from './components/statistic/statistic.component';
import { ReplayGameViewComponent } from './components/replay-game-view/replay-game-view.component';


export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    UnityWrapperComponent,
    HomeComponent,
    RegisteredComponent,
    ConfirmRegisterComponent,
    HeroesComponent,
    GamestartComponent,
    HeroComponent,
    GameViewComponent,
    ServerlobbyComponent,
    LobbyComponent,
    AccountpopoverComponent,
    RulesComponent,
    HomeComponent,
    AccountComponent,
    AlertComponent,
    NewProfileComponent,
    DetailedLobbyComponent,
    FacebookLoginComponent,
    StatisticComponent,
    ReplayGameViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['*']
      }
    }),

    NgbModule,
    NgbPopoverModule,
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: JwtInterceptor,
    multi: true
  }, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
