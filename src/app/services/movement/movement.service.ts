import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {LoginService} from '../login/login.service';
import {RangeRequest} from '../../models/RangeRequest';

@Injectable({
  providedIn: 'root'
})
export class MovementService {

  baseURL = environment.backendURL;

  constructor(private http: HttpClient, private loginService: LoginService) {
  }

  getRange(lobbyId: number) {
    const rangeRequest = new RangeRequest();
    rangeRequest.lobbyId = lobbyId;
    rangeRequest.userName = this.loginService.user.username;
    return this.http.post(this.baseURL + 'range', rangeRequest);
  }

}
