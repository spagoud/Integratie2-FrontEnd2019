import { TestBed } from '@angular/core/testing';

import { MovementService } from './movement.service';
import {HttpClientModule} from '@angular/common/http';
import {AppModule} from '../../app.module';

describe('MovementService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AppModule]}));

  it('should be created', () => {
    const service: MovementService = TestBed.get(MovementService);
    expect(service).toBeTruthy();
  });
});
