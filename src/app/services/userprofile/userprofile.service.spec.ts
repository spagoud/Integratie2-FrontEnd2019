import { TestBed } from '@angular/core/testing';

import { UserprofileService } from './userprofile.service';
import {HttpClientModule} from '@angular/common/http';
import {AppModule} from '../../app.module';

describe('UserprofileService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AppModule]}));

  it('should be created', () => {
    const service: UserprofileService = TestBed.get(UserprofileService);
    expect(service).toBeTruthy();
  });
});
