import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {LoginService} from '../login/login.service';
import {User} from '../../models/User';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserprofileService {

  baseURL = environment.backendURL + 'userprofile/';

  constructor(private http: HttpClient, private loginService: LoginService) {
  }

  getUserprofile() {
    return this.http.get(this.baseURL + this.loginService.user.username).pipe(map(response => response as User[]));
  }

  createUserProfile(userprofile: User) {
    return this.http.post(this.baseURL + 'newProfile/' + this.loginService.user.email, userprofile);
  }
}
