import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {LoginService} from '../login/login.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseURL = environment.backendURL + 'user/';

  constructor(private http: HttpClient, private loginService: LoginService) {}

  resetPassword() {
    let params = new HttpParams();
    params = params.append('email', this.loginService.user.email);
    return this.http.post(this.baseURL + 'resetPassword', null, { params: params }).pipe(map((response: Response) => {
      if (response.ok) {
        console.log('succes');
      }
      return response;
    }));
  }
}
