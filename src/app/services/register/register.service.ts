import {Injectable} from '@angular/core';
import {User} from '../../models/User';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private baseUrl = environment.backendURL + 'user/';

  constructor(private http: HttpClient) {
  }

  register(user: User) {
    return this.http.post(this.baseUrl + 'registration', user, {params: new HttpParams().set('redirectUrl', window.location.origin)});
  }

  confirm(token: string) {
    return this.http.get(this.baseUrl + 'registrationConfirm', {params: new HttpParams().set('token', token)});
  }
}
