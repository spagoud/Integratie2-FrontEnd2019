import { TestBed } from '@angular/core/testing';

import { UnitySenderService } from './unity-sender.service';
import {AppModule} from '../../app.module';

describe('UnitySenderService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AppModule]}));

  it('should be created', () => {
    const service: UnitySenderService = TestBed.get(UnitySenderService);
    expect(service).toBeTruthy();
  });
});
