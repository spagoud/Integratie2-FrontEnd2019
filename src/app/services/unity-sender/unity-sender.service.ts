import {Injectable} from '@angular/core';
import {Room} from '../../models/Room';
import {UnityRecieverService} from '../unity-reciever/unity-reciever.service';
import {GameLobby} from '../../models/GameLobby';
import {LoginService} from '../login/login.service';

declare var UnityLoader;

@Injectable({
  providedIn: 'root'
})
export class UnitySenderService {

  private gameInstance;

  constructor(private loginService: LoginService) {
  }

  startGame(callback: () => void) {
    this.gameInstance = UnityLoader.instantiate('gameContainer', 'assets/Unity/Build/Build.json');
    callback();
  }

  setVisibleRooms(rooms: Room[]) {
    this.gameInstance.SendMessage('AngularConnectionController', 'loadVisibleRooms', JSON.stringify(rooms));
  }

  setGame(game: string) {
    this.gameInstance.SendMessage('AngularConnectionController', 'StartGameResponce', game);
  }

  setUser() {
    console.log('SetUser angular: ' + this.loginService.user.username);
    this.gameInstance.SendMessage('AngularConnectionController', 'SetUser', this.loginService.user.username);
  }

  stopGame() {
    delete this.gameInstance;
  }

  SetRangeMovement(range: any) {
    this.gameInstance.SendMessage('AngularConnectionController', 'GetRangeResponse', JSON.stringify(range));
  }


  SendEvent(body: any) {
    this.gameInstance.SendMessage('AngularConnectionController', 'EventReceive', body);
  }

  SendChatMessage(chatMessage: any) {
    this.gameInstance.SendMessage('AngularConnectionController', 'ChatMessageResponse', chatMessage);
  }

  SendEnd(body: any) {
    this.gameInstance.SendMessage('AngularConnectionController', 'EndGame', body);
  }
}
