import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {GameLobby} from '../../models/GameLobby';
import {LoginService} from '../login/login.service';
import {map} from 'rxjs/operators';
import {User} from '../../models/User';


@Injectable({
  providedIn: 'root'
})
export class GamestartService {

  baseURL = environment.backendURL + 'gameLobby/';
  private heroName: string;

  constructor(private http: HttpClient, private loginService: LoginService) {
  }

  startLobby(gameLobby: GameLobby) {
    gameLobby.lobbyOwner = this.loginService.user.email;
    console.log(this.loginService.user);
    gameLobby.heroName = this.heroName;
    return this.http.post(this.baseURL + 'newGameLobby', gameLobby).pipe(map((json: any) => {
      console.log(json);
      return json as GameLobby;
    }));
  }

  setHero(heroName: string) {
    this.heroName = heroName;
    console.log('HeroName: ' + this.heroName);
  }
}
