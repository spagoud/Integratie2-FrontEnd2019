import {TestBed} from '@angular/core/testing';

import {GamestartService} from './gamestart.service';
import {HttpClientModule} from '@angular/common/http';
import {LoginService} from '../login/login.service';

describe('GamestartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule],
      providers: [LoginService]
    });
  });

  it('should be created', () => {
    const service: GamestartService = TestBed.get(GamestartService);
    expect(service).toBeTruthy();
  });
});
