import {TestBed} from '@angular/core/testing';

import {GamelogicService} from './gamelogic.service';
import {AppModule} from '../../app.module';

describe('GamelogicService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppModule]
  }));

  it('should be created', () => {
    const service: GamelogicService = TestBed.get(GamelogicService);
    expect(service).toBeTruthy();
  });
});
