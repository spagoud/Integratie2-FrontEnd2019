import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Door} from '../../models/Door';
import {map} from 'rxjs/operators';
import {GameLobby} from '../../models/GameLobby';


@Injectable({
  providedIn: 'root'
})
export class GamelogicService {
  baseURL = environment.backendURL + 'game/';

  constructor(private http: HttpClient) {
  }

  start(lobbyId: number) {
    return this.http.get(this.baseURL + 'lobby/' + lobbyId + '/start').pipe(map(object => object as GameLobby));
  }

  ready(lobbyId: number) {
    return this.http.get(this.baseURL + 'lobby/' + lobbyId + '/ready').pipe(map(object => object as boolean));
  }

  stop(lobbyId: number) {
    return this.http.get(this.baseURL + 'lobby/' + lobbyId + '/stop');
  }

  pauze(lobbyId: number) {
    return this.http.get(this.baseURL + 'lobby/' + lobbyId + '/pauze');
  }

  endTurn(lobbyId: number, heroName: string) {
    return this.http.post(this.baseURL + 'lobby/' + lobbyId + '/endTurn/', heroName);
  }

  openDoor(lobbyId: number, door: Door) {
    return this.http.post(this.baseURL + 'lobby/' + lobbyId + '/endTurn/', door);
  }
}
