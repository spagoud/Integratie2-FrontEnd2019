import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Hero} from '../../models/Hero';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  baseURL = environment.backendURL;

  constructor(private http: HttpClient) {
  }

  getHeroes() {
    return this.http.get(this.baseURL + 'hero').pipe(map(response => response as Hero[]));
  }
}
