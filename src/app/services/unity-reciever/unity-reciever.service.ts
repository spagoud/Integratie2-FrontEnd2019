import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ActionService} from '../action/action.service';
import {UnitySenderService} from '../unity-sender/unity-sender.service';
import {ActivatedRoute, Router} from '@angular/router';

// expose a window variable, later this wil be set to the instance of this service
export let _this = window['UnityReceiverService'];

@Injectable({
  providedIn: 'root'
})
export class UnityRecieverService {

  private gameStartRequestedSource = new BehaviorSubject(false);
  gameStartRequested = this.gameStartRequestedSource.asObservable();

  constructor(private actionservice: ActionService,
              private unitysenderservice: UnitySenderService,
              private router: Router) {
    // bind this service on window level
    _this = this;

    // bind all window methods to their respective methods in Angular.
    window['StartGame'] = this.StartGame;
    window['StopGame'] = this.StopGame;
    window['OpenDoor'] = this.OpenDoor;
    window['GetRange'] = this.GetRange;
    window['DoMove'] = this.DoMove;
    window['ChatRequest'] = this.ChatRequest;
    window['OpenChest'] = this.OpenChest;
    window['AttackMonster'] = this.AttackMonster;
  }

  // In all methods, refer to the exposed instance of this class.
  // The methods get called from the window, thus don't know the class variables with the normal this.

  private StartGame() {
    _this.gameStartRequestedSource.next(true);
  }

  private StopGame() {
    console.log('EndGame A');
    _this.unitysenderservice.stopGame();
    _this.router.navigate('/gameRooms');
  }

  private OpenDoor(doorActionJson: string) {
    console.log('door A: ' + doorActionJson);
    _this.actionservice.openDoor(JSON.parse(doorActionJson)).subscribe();
  }

  private GetRange(heroName: string) {
    console.log('hero: ' + heroName);
    _this.actionservice.getRange(heroName).subscribe((json) =>
      _this.unitysenderservice.SetRangeMovement(json));
  }

  private DoMove(moveActionJson: string) {
    console.log('move A: ' + moveActionJson);
    _this.actionservice.doMove(JSON.parse(moveActionJson)).subscribe();
  }

  private ChatRequest(chatMessage: string) {
    console.log('chat A: ' + chatMessage);
    _this.actionservice.SendChatMessage(chatMessage).subscribe();
  }

  private OpenChest(openChestActionJson: string) {
    console.log('open chest A: ' + openChestActionJson);
    _this.actionservice.OpenChestREquest(JSON.parse(openChestActionJson)).subscribe();
  }

  private AttackMonster(AttackMonsterJson: string) {
    console.log('attack monster A: ' + AttackMonsterJson);
    _this.actionservice.AttackMonsterRequest(JSON.parse(AttackMonsterJson)).subscribe();
  }
}
