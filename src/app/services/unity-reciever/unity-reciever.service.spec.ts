import { TestBed } from '@angular/core/testing';

import { UnityRecieverService } from './unity-reciever.service';
import {AppModule} from '../../app.module';

describe('UnityRecieverService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AppModule]}));


  it('should be created', () => {
    const service: UnityRecieverService = TestBed.get(UnityRecieverService);
    expect(service).toBeTruthy();
  });
});
