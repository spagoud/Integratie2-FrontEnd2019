import { TestBed } from '@angular/core/testing';

import { WebsocketService } from './websocket.service';
import {AppModule} from '../../app.module';

describe('WebsocketService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AppModule]}));

  it('should be created', () => {
    const service: WebsocketService = TestBed.get(WebsocketService);
    expect(service).toBeTruthy();
  });
});
