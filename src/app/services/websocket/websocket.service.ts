import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

const SockJs = require('sockjs-client');

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  baseURL = environment.socketURL;

  constructor(private Http: HttpClient) {
  }

  public Test() {
    this.Http.get('http://127.0.0.1:8081/broadcast/test/1').subscribe();
  }
}
