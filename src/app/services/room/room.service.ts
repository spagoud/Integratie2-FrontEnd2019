import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Room} from '../../models/Room';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  baseURL = environment.backendURL;

  constructor(private http: HttpClient) {
  }

  getFirstRoom() {
    return this.http.get(this.baseURL + 'room/testRoom').pipe(map(responce => responce as Room));
  }
}
