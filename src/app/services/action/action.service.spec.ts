import { TestBed } from '@angular/core/testing';

import { ActionService } from './action.service';
import {AppModule} from '../../app.module';

describe('ActionService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AppModule]}));

  it('should be created', () => {
    const service: ActionService = TestBed.get(ActionService);
    expect(service).toBeTruthy();
  });
});
