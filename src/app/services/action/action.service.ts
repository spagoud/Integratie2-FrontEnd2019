import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {GameLobby} from '../../models/GameLobby';
import {DoorAction} from '../../models/actions/DoorAction';
import {ResponseType} from '@angular/http';
import {MoveAction} from '../../models/actions/MoveAction';

@Injectable({
  providedIn: 'root'
})
export class ActionService {
  baseURL = environment.backendURL + 'action/';
  private lobbyId: string;

  constructor(private http: HttpClient) { }

  openDoor(doorAction: string) {
    return this.http.post(this.baseURL + 'lobby/' + this.lobbyId + '/openDoor', doorAction);
  }

  setLobbyId(lobbyId: string) {
    this.lobbyId = lobbyId;
    console.log('lobbyId: ' + this.lobbyId);
  }

  getRange(heroName: string) {
    return this.http.post(this.baseURL  + 'lobby/' + this.lobbyId + '/range', heroName);
  }

  doMove(moveAction: MoveAction) {
    return this.http.post(this.baseURL  + 'lobby/' + this.lobbyId + '/move', moveAction);
  }

  SendChatMessage(chatMessage: string) {
    return this.http.post(environment.broadcastURL  + this.lobbyId, chatMessage);
  }

  OpenChestREquest(openChestAction: string) {
    return this.http.post(this.baseURL  + 'lobby/' + this.lobbyId + '/openChest', openChestAction);
  }

  AttackMonsterRequest(attackMonsterAction: string) {
    return this.http.post(this.baseURL  + 'lobby/' + this.lobbyId + '/attack', attackMonsterAction);
  }
}
