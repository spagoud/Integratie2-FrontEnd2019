import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from '../../models/User';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseURL = environment.backendURL;

  constructor(private http: HttpClient, private router: Router) {
    const user = JSON.parse(localStorage.getItem('userprofile'));
    if (user) {
      this.user = user;
    } else {
      this.user = null;
      localStorage.removeItem('token');
    }
  }

  private _user: User;

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
    localStorage.setItem('userprofile', JSON.stringify(value));
  }

  login(user: User) {
    const data = new FormData();
    data.append('username', user.email);
    data.append('password', user.password);
    data.append('code', '');
    this.http.post(this.baseURL + 'login', data, {observe: 'response' as 'body'}).pipe(map((resp: HttpResponse<any>) => {
      console.log(resp);
      if (resp.headers.has('Authorization')) {
        const token = resp.headers.get('Authorization').substr(7);
        localStorage.setItem('token', token);
        return this.getUserProfile(user.email).subscribe(
          result => {
            if (result) {
              this.router.navigate(['/home']);
            } else {
              this.router.navigate(['/newProfile']);
            }
          }
        );
        // if (userprofile == null) {
        //   this.router.navigate(['/newProfile']);
        // } else {
        //   this.router.navigate(['/']);
        // }
      }
    })).subscribe();
  }

  // TODO: redirect link aanpassen zodat dit algemeen kan worden tov omgeving!
  loginWithFacebook() {
    return this.http.get(this.baseURL + 'facebook/login',
      {params: new HttpParams().set('redirectUrl', 'http://localhost:4200/facebook')}).pipe(map((response: HttpResponse<any>) => {
      return response.url;
    }));
  }

  authenticateWithFacebook(code: string) {
    return this.http.get(this.baseURL + 'facebook',
      {params: new HttpParams().set('code', code), observe: 'response' as 'body'}).pipe(map((response: HttpResponse<any>) => {
      if (response.headers.has('Authorization')) {
        const token = response.headers.get('Authorization').substr(7);
        localStorage.setItem('token', token);
        return this.getUserProfile(response.headers.get('username')).subscribe(
          result => {
            if (result) {
              this.router.navigate(['/home']);
            } else { const user = new User();
              user.email = response.headers.get('username');
              this.user = user;
              localStorage.setItem('userprofile', JSON.stringify(user));
              this.router.navigate(['/newProfile']);
            }
          }
        );
      }
    })).subscribe();
  }

  loginWithGoogle() {

  }

  loginWithLinkedin() {

  }

  logout() {
    return this.http.post(this.baseURL + 'logout', null, {observe: 'response' as 'body'}).pipe(map((response: Response) => {
      if (response.ok) {
        localStorage.removeItem('token');
        localStorage.removeItem('userprofile');
        this.user = null;
      }
    }));
  }

  getUserProfile(email: string) {
    return this.http.post(this.baseURL + 'userprofile', email).pipe(map(
      (response => {
        const user = response as User;
        if (user != null) {
          user.email = email;
          this.user = user;
          localStorage.setItem('userprofile', JSON.stringify(user));

        }
        return user;
      })
    ));
  }
}
