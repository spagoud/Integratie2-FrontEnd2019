import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {LoginService} from '../login/login.service';
import {UserStatistics} from '../../models/UserStatistics';

@Injectable({
  providedIn: 'root'
})
export class StatisticService {

  baseURL = environment.backendURL + 'userprofile/';
  loggedInUser;

  constructor(private http: HttpClient, private loginService: LoginService) {
    this.loggedInUser = loginService.user.email;
  }

  getStatics() {
    return this.http.get(this.baseURL + 'statistics', {params: new HttpParams().set('email', this.loggedInUser)});
  }
}
