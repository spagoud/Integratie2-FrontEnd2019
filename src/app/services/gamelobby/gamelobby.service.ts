import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {GameLobby} from '../../models/GameLobby';
import {JoinLobby} from '../../models/JoinLobby';

@Injectable({
  providedIn: 'root'
})
export class GamelobbyService {

  baseURL = environment.backendURL + 'gameLobby/';

  constructor(private http: HttpClient) {
  }

  getAllLobbies() {
    return this.http.get(this.baseURL + 'serverLobby').pipe(map(response => response as GameLobby[]));
  }

  joinLobby(lobbyId: string, email: string) {
    return this.http.post(this.baseURL + 'joinLobby/' + lobbyId, null, {params: new HttpParams().set('email', email)}).pipe();
  }

  addPlayer(lobbyId: string, joinLobby: JoinLobby) {
    return this.http.post(this.baseURL + 'addPlayer/' + lobbyId, joinLobby).pipe();
  }

  removeViewer(lobbyId: string, email: string) {
    return this.http.post(this.baseURL + 'removeViewer/' + lobbyId, null, {params: new HttpParams().set('email', email)}).pipe();
  }

  getLobby(id: string) {
    return this.http.get(this.baseURL + id).pipe(map((json: any) => {
      return json as GameLobby;
    }));
  }

  remove(lobbyId: string) {
    return this.http.post(this.baseURL + 'removeLobby/' + lobbyId, null).pipe();
  }

  getReplay(lobbyId: number) {
    return this.http.get(this.baseURL + 'replay/' + lobbyId);
  }
}
