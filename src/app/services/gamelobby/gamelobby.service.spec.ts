import { TestBed } from '@angular/core/testing';

import { GamelobbyService } from './gamelobby.service';
import {HttpClientModule} from '@angular/common/http';
import {AppModule} from '../../app.module';

describe('GamelobbyService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AppModule]}));

  it('should be created', () => {
    const service: GamelobbyService = TestBed.get(GamelobbyService);
    expect(service).toBeTruthy();
  });
});
