import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {AppModule} from '../../app.module';

describe('AuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [
      AppModule
    ]}));

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });
});
