import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() {
  }

  // ...
  public isAuthenticated(): boolean {
    return localStorage.hasOwnProperty('token');
    // Check whether the token is expired and return
    // true or false
    // return !!token; // !this.jwtHelper.isTokenExpired(token);
  }
}
