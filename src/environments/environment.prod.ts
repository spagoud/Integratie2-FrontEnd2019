export const environment = {
  production: true,
  // backendURL: process.env.BACKEND_URL
  backendURL: 'https://integratie2-dev-backend2019.herokuapp.com/',
  socketURL: 'https://integratie2-broadcaster.herokuapp.com/ws/',
  broadcastURL: 'https://integratie2-broadcaster.herokuapp.com/chat/sendMessage/'
};
