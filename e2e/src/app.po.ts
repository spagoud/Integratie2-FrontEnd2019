import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getBody() {
    return element(by.tagName('body'));
  }
}
